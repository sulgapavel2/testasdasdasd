package com.hansab.perh;

import org.apache.log4j.Logger;

class MessageDispatcher {

    private static final Logger logger = Logger.getLogger(MessageDispatcher.class);
    private static final String URL = "https://c116.by.enlife.io/sites?id=3/devices?id=13";

    void dispatchDeviceStatuses(String requestBody) {
        HttpUtil.HttpResultDTO result = HttpUtil.sendPost(requestBody, URL);
        int HTTP_RESULT_OK = 200;
        if (result.getStatus() != HTTP_RESULT_OK) {
            logger.info("Message is not being sent...");
        } else {
            logger.info("Message is successfully sent!");
        }
    }
}

