package com.hansab.perh;

import com.sun.mail.imap.protocol.FLAGS;
import org.apache.log4j.Logger;

import javax.mail.*;
import javax.mail.search.FlagTerm;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Properties;

import static javax.mail.Folder.READ_ONLY;
import static javax.mail.Folder.READ_WRITE;

public class CheckingMails {

    private static final Logger logger = Logger.getLogger(CheckingMails.class);

    static Message[] getNewEmails(String host, String storeType, String user,
                                  String password, String deleteEmails) throws IOException, MessagingException {

        Properties configProperties = new Properties();
        FileInputStream ip = new FileInputStream("config.properties");
        configProperties.load(ip);

        Properties properties = new Properties();
        properties.put("mail.pop3.host", host);
        properties.put("mail.pop3.port", configProperties.getProperty("port"));
        properties.put("mail.pop3.starttls.enable", configProperties.getProperty("enableSSL"));

        Session emailSession = Session.getDefaultInstance(properties);

        Store store = emailSession.getStore("pop3s");

        store.connect(host, user, password);

        Folder inbox = store.getFolder("INBOX");

        if(deleteEmails.equals("true")){
            inbox.open(READ_WRITE);
        }
        else if (deleteEmails.equals("false")) {
            inbox.open(READ_ONLY);
        }

        Flags seen = new Flags(Flags.Flag.SEEN);
        FlagTerm unseenFlagTerm = new FlagTerm(seen, false);
        Message[] messages = inbox.search(unseenFlagTerm);

        return messages;
    }

    static void deleteNewEmails(String host, String storeType, String user,
                                String password, String deleteEmails) throws MessagingException, IOException {

        Message[] messages = getNewEmails(host, storeType, user, password, deleteEmails);

        if (messages.length != 0){
            Arrays.sort(messages, (m1, m2) -> {
                try {
                    return m2.getSentDate().compareTo(m1.getSentDate());
                } catch (MessagingException e) {
                    logger.error("Error occurred: " + e);
                    throw new RuntimeException(e);
                }
            });
            // Deleting every message after it gets checked
            for (Message message : messages) {
                message.setFlag(FLAGS.Flag.DELETED, true);
            }
        } else {
            logger.info("No new messages found");
        }
    }

    public static void main(String[] args) throws IOException, MessagingException {

        logger.info("Connection is successful");
        logger.info("Connection time");
        DeviceManager deviceManager = new DeviceManager();
        ConnectionConfiguration.configureConnection();

        while (true) {
            try {
                deviceManager.processEmails();
                Thread.sleep(15000);
                logger.info("Scanning for new emails...");
            } catch (Exception e) {
                logger.error(e);
            }
        }
    }
}