package com.hansab.perh;

import javax.mail.Message;
import javax.mail.MessagingException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

class Pop3Manager {

    Message[] getUnreadEmailSubjects() throws IOException, MessagingException {

        Properties prop = new Properties();
        FileInputStream ip = new FileInputStream("config.properties");
        prop.load(ip);

        Message[] messages;
        messages = (CheckingMails.getNewEmails(
                prop.getProperty("host"),
                prop.getProperty("mailStoreType"),
                prop.getProperty("username"),
                prop.getProperty("password"),
                prop.getProperty("deleteEmails")));

        return messages;
    }
}
