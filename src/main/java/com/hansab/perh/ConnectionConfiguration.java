package com.hansab.perh;

import org.apache.log4j.Logger;

import javax.mail.MessagingException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

class ConnectionConfiguration {

    private static final Logger logger = Logger.getLogger(ConnectionConfiguration.class);

    static void configureConnection() throws IOException, MessagingException {

        Properties prop = new Properties();
        FileInputStream ip = null;
        try {
            ip = new FileInputStream("config.properties");
        } catch (FileNotFoundException e) {
            logger.error("Error occurred: " + e);
            e.printStackTrace();
        }
        try {
            assert ip != null;
            prop.load(ip);
        } catch (IOException e) {
            logger.error("Error occurred: " + e);
            e.printStackTrace();
        }

        String host = prop.getProperty("host");// change accordingly
        String mailStoreType = prop.getProperty("mailStoreType");
        String username = prop.getProperty("username");// change accordingly
        String password = prop.getProperty("password");// change accordingly

        CheckingMails.deleteNewEmails(host, mailStoreType, username, password, prop.getProperty("deleteEmails"));
    }
}
