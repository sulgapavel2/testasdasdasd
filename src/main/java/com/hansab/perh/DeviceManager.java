package com.hansab.perh;


import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.mail.Message;
import javax.mail.MessagingException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

class DeviceManager {

    private static final Logger logger = Logger.getLogger(DeviceManager.class);
    private Pop3Manager pop3Manager = new Pop3Manager();
    private MessageDispatcher messageDispatcher = new MessageDispatcher();

    void processEmails() throws IOException, MessagingException {

        Message[] emailSubjects = pop3Manager.getUnreadEmailSubjects();

        List<String> errors = new ArrayList<>();
        List<String> errorCodes = new ArrayList<>();

        for (Message message : emailSubjects) {
            // Getting errorCode and DeviceIndex from message subject
            String errorCode = null;
            try {
                errorCode = message.getSubject().split(",")[0];
            } catch (ArrayIndexOutOfBoundsException e) {
                logger.error("Incompatible subject format! Check the syntax!");
            }
            String deviceIndex = null;
            try {
                deviceIndex = message.getSubject().split(", ")[1];
            } catch (ArrayIndexOutOfBoundsException e) {
                logger.error("Incompatible subject format! Check the syntax!");
            }

            // Applying JSON config
            JSONObject cfg = null;
            cfg = getConfig();
            JSONObject devices = cfg.getJSONObject("deviceNumberMapping");

            // Checking if deviceIndex that came from mail exists in deviceNumberMapping list to map it with device number
            // and if it's not -> adding the one that came from mail to the errors.list
            if (!devices.has(deviceIndex + "")) {
                if (errorCodes.indexOf(errorCode) == -1) {
                    errorCodes.add(errorCode);
                    errors.add("{\"io" + errorCode + "\":true, \"device\":\"" + deviceIndex +"\"" + "}");
                }
            } else {
                if (errorCodes.indexOf(errorCode) == -1) {
                    assert deviceIndex != null;
                    String device = devices.get(deviceIndex).toString();
                    errorCodes.add(errorCode);
                    errors.add("{\"io" + errorCode + "\":true, \"device\":\"" + device +"\"" + "}");
                }
            }
        }

        String requestBody = "{\"status\":" + errors + "}";
        int noOfErrors = errors.size();

        logger.info("Request posted: " + requestBody);
        logger.info("Number of errors found: " + noOfErrors);

        messageDispatcher.dispatchDeviceStatuses(requestBody);
    }

    private JSONObject getConfig() {
        JSONParser parser = new JSONParser();

        File file = new File("devices.json");
        String contents = readFileContent(file.toPath());
        JSONObject cfg = new JSONObject(contents);
        return cfg;
    }

    private static String readFileContent(Path file){
        String content = "";
        try {
            content = new String(Files.readAllBytes(file));
        } catch (IOException e) {
            logger.error(e);
        }
        return content;
    }
}
