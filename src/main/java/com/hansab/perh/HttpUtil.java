package com.hansab.perh;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

class HttpUtil {

    private static final Logger logger = Logger.getLogger(HttpUtil.class);
    private static final  String USER_AGENT = "Mozilla/5.0";

    static HttpResultDTO sendPost(String requestBody, String url){
        HttpResultDTO responseDTO;
        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection ) obj.openConnection();

            //add request header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setRequestProperty("Content-Type", "application/xml");

            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(requestBody);
            wr.flush();
            wr.close();

            int responseCode = con.getResponseCode();

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            HttpResultDTO.Result result = responseCode == 200 ? HttpResultDTO.Result.SUCCESS : HttpResultDTO.Result.REJECTED;
            responseDTO = new HttpResultDTO(result, responseCode, response.toString());
        } catch (IOException e) {
            HttpResultDTO.Result result = HttpResultDTO.Result.UNKNOWN;
            responseDTO = new HttpResultDTO(result, 0, e.getMessage());
            logger.error(e);

        }
        return responseDTO;
    }

    public static final class HttpResultDTO {
        public enum Result { SUCCESS, REJECTED, UNKNOWN,}
        private Result result;
        private int status;
        private String message;

        HttpResultDTO(Result result, int status, String message) {
            this.result = result;
            this.status = status;
            this.message = message;
        }

        public Result getResult() {
            return result;
        }

        int getStatus() {
            return status;
        }

        public String getMessage() {
            return message;
        }

        @Override
        public String toString() {
            return "HttpResultDTO{" +
                    "result=" + result +
                    ", status=" + status +
                    ", message='" + message + '\'' +
                    '}';
        }
    }
}
